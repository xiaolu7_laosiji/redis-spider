package com.lagou.cluster.service;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.io.ByteArrayInputStream;
import java.util.List;

@Component
public class ItstyleProcessor implements PageProcessor, ApplicationContextAware {
    private FastFileStorageClient fastFileStorageClient;
    private RMap<String, String> fastDfsMap;

    @Autowired
    public void setRedissonClient(RedissonClient client) {
       this.fastDfsMap = client.getMap("map_itstyle", new StringCodec());
    }

    @Override
    public void process(Page page) {
        byte[] data = page.getBytes();
        StorePath path = fastFileStorageClient.uploadFile(new ByteArrayInputStream(data),data.length,
                "html",null);
        fastDfsMap.put(page.getUrl().get(), path.getFullPath());
        List<String> next = page.getHtml().links().regex("https://blog.52itstyle.vip/archives/\\d+").all();
        page.addTargetRequests(next);
        page.putField("title", page.getHtml().xpath("//*[@id=\"posts\"]/article/header/h1"));
    }

    @Override
    public Site getSite() {
        return Site.me().setDomain("https://blog.52itstyle.vip")
                .setSleepTime(5000);
    }

    //相当于延迟初始化fastdfs
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.fastFileStorageClient = applicationContext.getBean(FastFileStorageClient.class);
    }
}
