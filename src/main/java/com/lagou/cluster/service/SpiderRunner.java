package com.lagou.cluster.service;

import com.github.tobato.fastdfs.conn.TrackerConnectionManager;
import com.github.tobato.fastdfs.service.DefaultTrackerClient;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import us.codecraft.webmagic.Spider;

import java.io.ByteArrayInputStream;

@Service
public class SpiderRunner implements CommandLineRunner {
    @Autowired
    private Spider spider;
    @Override
    public void run(String... args) throws Exception {
        spider.run();
    }
}
