package com.lagou.cluster.config;

import us.codecraft.webmagic.Request;

import java.util.Queue;
import java.util.Set;

public class RedissonScheduler extends AbstractScheduler {
	
	public RedissonScheduler(Queue<Request> queue, Set<String> history) {
		super(queue, history);
	}
}