//package com.lagou.cluster.web;
//
//import org.redisson.api.RBucket;
//import org.redisson.api.RBuckets;
//import org.redisson.api.RedissonClient;
//import org.redisson.client.codec.StringCodec;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.stream.IntStream;
//
//@RestController
//@RequestMapping("/example")
//public class ExampleController {
//
//    @Autowired
//    private RedissonClient redissonClient;
//
//    @RequestMapping("/batch")
//    public Object batch() {
//        IntStream.range(5001, 6001).forEach(index -> {
//           redissonClient.getBucket("lagou" + index, new StringCodec()).set("this is value");
//        });
//        return 1;
//    }
//
//    @RequestMapping("/batchGet")
//    public Object batchGet() {
//        IntStream.range(1, 1001).forEach(index -> {
//            Object r = redissonClient.getBucket("lagou" + index, new StringCodec()).get();
//            if (r == null) {
//                System.out.println(index);
//            }
//        });
//        return 1;
//    }
//}
