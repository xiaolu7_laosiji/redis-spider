本项目，主要是用于演示redis各种数据结构应用。

参考资料

http://webmagic.io/docs/zh/

https://github.com/redisson/redisson/wiki

https://blog.52itstyle.vip/

我们准备对52itstyle这个网站进行爬取，注意，这个项目只是用于演示，请同学们不要拿去干坏事。。。

# 爬虫流程

一个正常的爬虫流程，首先，选取一个初始页面，比如

https://blog.52itstyle.vip/

获取这个页面上所有的链接（也就是A标签），将链接加入待爬取队列。

之后，从队列中获取一个链接（通过poll方法）。

之后，通过HttpClient或其他工具，获取这个页面的html源代码。

之后，对这个html页面进行一些处理（ETL）。

上面步骤进行完成后，可以将url加入已爬取队列。

加入已爬取url列表的意义，就是为了防止重复爬取。

## 名词解释

**待爬取队列**

通俗的讲：假如A页面，A里面有10个链接，这10个链接都可以称为待爬取队列。

一般的，我们可以用java中的java.util.Queue代表这个数据结构。

那么java.util.Queue，对应的就是redis的list数据结构。

注意，一般在互联网爬虫中，获取链接的过程，属于一个递归的过程。

A页面，包含10个链接，这10个链接里面，又包含其他的链接。

**已爬取url**

通俗的讲：我们对一个url对应的html内容处理完成后 (比如我们可以把html内容存进mysql)

那么在我们存入mysql成功后，要标记这个url为已爬取。

我们可以用java.util.Set代表这个数据结构。

那么java.util.Set，对应的就是redis的set数据结构。

# 项目介绍

## 重要类说明

**com.lagou.cluster.config.AbstractScheduler**

爬虫调度类，封装了redis将url入队，出队，以及加入历史url的过程。

**com.lagou.cluster.config.RedissonConfig**

配置redis，项目使用redisson框架对redis集群进行操作。

## 项目流程解析

**com.lagou.cluster.service.ItstyleProcessor**

这里面，有个process方法（这个方法，会递归调用），执行流程如下：

1.获取当前正在爬取的url。

2.首先，将url对应的html的原始文件，存入fastdfs。

3.使用redis的hash结构，存储url和 fastdfs的fileId的 对应关系。

4.解析html原始文件。

5.解析html原始文件的标题字段，并打印

6.将爬取过的url加入redis set集合。

7.使用正则表达式获取html原始文件的所有链接。

8.将上一步获取的链接加入redis队列，递归的进行下一次操作。

